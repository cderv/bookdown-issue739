#!/bin/sh

set -ev

wget -O pandoc-$1-1-amd64.deb https://github.com/jgm/pandoc/releases/download/$1/pandoc-$1-1-amd64.deb
dpkg -i pandoc-$1-1-amd64.deb

Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook', output_dir = 'public/pandoc-$1')"
